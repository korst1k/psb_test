#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

n = 100

if len(sys.argv) > 1:
    n = int(sys.argv[1])

#min_lc, len_lc = ord(b'a'), ord(b'z') + 1 - ord(b'a')   # lowercase alphabet only
#min_lc, len_lc = ord(b'!'), ord(b'~') + 1 - ord(b'!')   # if need full set of symbols - uncomment this line

ba = bytearray(os.urandom(n))
#for i, b in enumerate(ba):
#    ba[i] = min_lc + b % len_lc

with open('sequence.txt', 'w') as f:
    f.write(ba)

#print ba

