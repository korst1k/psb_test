#include <memory>
#include "process.hpp"


int main(int argc, char* argv[])
{
    std::unique_ptr<app::process_t> process = nullptr;

    process.reset(new app::process_t(argc, argv));
    process->start();

    return 0;
}

