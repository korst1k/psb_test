#include "process.hpp"
#include "radix_sort.hpp"
#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <algorithm>
#include <stdexcept>
#include <limits>


namespace app
{
    process_t::process_t(int argc, char* argv[])
    {
        if (argc > 3)
        {
            _fname = argv[1];
            _max_memory = std::atol(argv[2]);
            _max_threads = std::atol(argv[3]);
        }
    }

    process_t::~process_t()
    {
        stop();
    }

    void process_t::start()
    {
        if (!_is_run)
        {
            _is_run = true;

            processing_loop();
        }
    }

    void process_t::stop()
    {
        _is_run = false;
    }

    void process_t::processing_loop()
    {
        _file.open(_fname, std::ios::ate);

        if (_file.is_open())
        {
            const size_t fsize = _file.tellg();
            const size_t chunksize = (_max_threads > 0)
                                     ? _max_memory / _max_threads / 3   // 3 - coef for radix sort alg
                                     : _max_memory;

            _file.seekg(std::ios_base::beg);

            std::vector<size_t> positions;
            for (auto pos = 0; pos < fsize; pos += chunksize)
            {
                positions.push_back(pos);
            }

            int i = 0;
            std::vector<std::vector<size_t>> positions_by_thread(_max_threads);
            for (const auto& pos : positions)
            {
                positions_by_thread[i].push_back(pos);

                i++;
                if (i == _max_threads)
                {
                    i = 0;
                }
            }

            std::vector<std::thread> threads;
            for (const auto& item : positions_by_thread)
            {
                threads.push_back(std::thread([this, item, chunksize, fsize]()
                {
                    for (size_t pos : item)
                    {
                        const size_t bytes_rest = fsize - pos;
                        const size_t length = (bytes_rest < chunksize) ? bytes_rest : chunksize;

                        std::string slice(length, '\0');

                        {
                            std::lock_guard<std::mutex> lock(_mtx);

                            if (_file.good())
                            {
                                _file.seekg(pos);
                                _file.read(&slice[0], length);
                            }
                            else
                            {
                                throw std::logic_error("file stream bad");
                            }
                        }

                        radix_sort_char(&slice[0], length);

                        // save sorted slice to file
                        std::string fname = std::to_string(pos) + ".txt";
                        std::ofstream outfile(fname);
                        outfile << slice;
                    }
                }));
            }

            // waiting for worker threads
            for (auto& item : threads)
            {
                if (item.joinable())
                {
                    item.join();
                }
            }

            // open files
            filelist_t files;
            for (const auto& pos : positions)
            {
                std::string fname = std::to_string(pos) + ".txt";
                files.emplace_back(fname, std::ios_base::in);
            }

            std::vector<char> buffer(_max_memory / 2, '\0');

            // merging loop
            std::ofstream result_file("result.txt", std::ios_base::out);

            char min_c = find_min(files);
            size_t bc = 0;

            while (!files.empty())
            {
                size_t miss_count = 0;

                // pass through all files
                for (auto i = files.begin(); i != files.end();)
                {
                    if (i->good())
                    {
                        const char c = i->get();

                        if (c == min_c)
                        {
                            buffer[bc++] = c;

                            if (bc == buffer.size())
                            {
                                result_file.write(&buffer[0], bc);
                                bc = 0;
                            }
                        }
                        else
                        {
                            i->unget();

                            miss_count++;

                            if (files.size() == miss_count)
                            {
                                min_c = find_min(files);    // find new min
                            }
                        }

                        ++i;
                    }
                    else
                    {
                        i = files.erase(i);
                    }
                }
            }

            // write the reminder
            if (bc > 0)
            {
                result_file.write(&buffer[0], bc);
            }
        }
        else
        {
            std::cout << "file not opened" << std::endl;
        }
    }

    char process_t::find_min(filelist_t& files)
    {
        // find min char
        char min_c = std::numeric_limits<char>::max();

        if (!files.empty())
        {
            for (auto& file : files)
            {
                if (file.good())
                {
                    char cc = file.peek();
                    if (cc < min_c)
                    {
                        min_c = cc;
                    }
                }
            }
        }
        else
        {
            min_c = 0;
        }

        return min_c;
    }
}
