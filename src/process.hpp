#ifndef PROCESS_HPP
#define PROCESS_HPP

#include <string>
#include <list>
#include <thread>
#include <fstream>
#include <mutex>


namespace app
{
    class process_t final
    {
    public:
        process_t(int argc, char* argv[]);
        ~process_t();

        process_t(const process_t&) = delete;
        process_t(process_t&&) = delete;

        process_t& operator=(const process_t&) = delete;
        process_t& operator=(process_t&&) = delete;

        void start();
        void stop();

    private:
        using filelist_t = std::list<std::ifstream>;

        void processing_loop();

        static char find_min(filelist_t& file);

        bool _is_run = false;

        // 1111 Mb = 1164967936 bytes
        // 255 Mb =  267386880 bytes

        // params
        std::string _fname = "sequence_big.txt";
        uint32_t _max_threads = std::thread::hardware_concurrency();
        uint32_t _max_memory = 267386880;

        std::mutex _mtx;
        std::ifstream _file;
    };
}

#endif
