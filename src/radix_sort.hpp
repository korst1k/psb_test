#ifndef RADIX_SORT_HPP
#define RADIX_SORT_HPP

#include <unistd.h>
#include <cstring>


namespace app
{
    void radix_sort_char(char* bytes, size_t length)
    {
        char* ones_array = (char*)std::malloc(length);
        char* zeros_array = (char*)std::malloc(length);
        int ones;
        int zeros;
        int mask = 1;

        for (size_t count = 0; count < 8; count++)
        {
            ones = zeros = 0;

            for (size_t i = 0; i < length; i++)
            {
                char bt = bytes[i];
                if (bt & mask)
                {
                    ones_array[ones] = bt;
                    ones++;
                }
                else
                {
                    zeros_array[zeros] = bt;
                    zeros++;
                }
            }

            // copy zeros
            std::memcpy(bytes, zeros_array, zeros);

            // copy ones
            std::memcpy(bytes + zeros, ones_array, ones);

            // next bit check
            mask <<= 1;
        }

        std::free(ones_array);
        std::free(zeros_array);
    }
}

#endif // RADIX_SORT_HPP
